# OpenML dataset: FEER-Dataset

https://www.openml.org/d/43602

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This database contains six basic emotions (happiness, surprise, anger, fear, disgust, and sadness) of normalized (average mean reference) data and collected from 85 undergraduate university students (55 male; 30 female) aged between 20 - 27 years with a mean age of 24.5 years. The data which contain noises and other movement artifacts are removed from the raw data. A built-in face time HD camera in Apple Mac Pro with a resolution of 2560  1600 at 227 pixels per inch is used to collect the facial images in a controlled environment (25C room temperature with 50 Lux lighting intensity) at 30 frames per second. All the subjects are seated comfortably in a chair in front of the camera and the distance between the subject face to the camera is 0.95m. A computerized PowerPoint slides are used to instruct the subjects to express the facial emotional expression by looking into the International Affective Picture System (IAPS) images of six different emotions. The data file contains 11 columns (10 columns for 10 markers and the last column represents the label of emotion) and 190968 rows. In the file (labels), 0 refers to Angry, 1 refers to Disgust, 2 refers to Fear, 3 refers to Sad, 4 refers to Happy, and 5 refer to S to Surprise.  Each emotion has 10 trials and each trial has a duration of 6 sec. In between the emotional expressions, 10 sec of break is given to the subjects to feel calm by showing natural scenes. The PowerPoint show starts with a set of instructions at the beginning of the experiment of 10-sec duration.  The computing system continuously records the marker positions and saved them in comma-separated values (CSV) format for further processing. More information about the reproduction of data analysis could be found from the below citations. 
Citations
All documents and papers that report on research that uses the FEER Dataset must acknowledge the use of the database by including a citation

M Murugappan, Vasanthan Maruthapillai, Wan Khairunizam, A M Muttawa, Sai Sruthi, Wen Yean, Virtual Markers based Facial Emotion Recognition using ELM and PNN Classifiers, 16th IEEE Colloquium on Signal Processing, (CSPA), pp, 261-265, 2020.
Vasanthan, M Murugappan, Optimal Geometrical Set for Automated Marker Placement to Virtualized Real-Time Facial Emotions, PlosOne, 11(2), Feb 2016. DOI: 10.1371/journal.pone.0149003.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43602) of an [OpenML dataset](https://www.openml.org/d/43602). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43602/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43602/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43602/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

